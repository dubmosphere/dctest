<?php
$startTime = round(microtime(true) * 1000000);
/**
 * Dub MVC
 * If not working, use PHP 5.3 or higher.
 */
define('DS', DIRECTORY_SEPARATOR); // Shortcut for the DIRECTORY_SEPARATOR constant
define('DOCUMENT_ROOT', ''); // The relative path back to the root of the main web application

// Include the frontend application class (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms' . DS . 'syspkg' . DS . 'frontend' . DS
	. 'classes' . DS . 'Application' . DS . 'FrontendApplication.php';
	
// Create the application and run it
$app = new \Dub\Frontend\Application\FrontendApplication();
$app -> run();

$endTime = round(microtime(true) * 1000000);
echo $endTime - $startTime . '<br>';
