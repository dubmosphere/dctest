<?php
$defaultPkgFolder = 'dub-cms-conf/pkg/';
$defaultClassesFolder = 'classes';

// Here you can change the order of the package loading proccess
return [
	// Needed packages
	'core' => [
		'packageFolder' => 'dub-cms/syspkg/core',
		'classesFolder' => $defaultClassesFolder,
		'active' => true
	],
	'frontend' => [
		'packageFolder' => 'dub-cms/syspkg/frontend',
		'classesFolder' => $defaultClassesFolder,
		'active' => true
	],
	'template' => [
		'packageFolder' => $defaultPkgFolder . 'template',
		'classesFolder' => $defaultClassesFolder,
		'active' => true
	],

	// Custom packages
	'onegram' => [
		'packageFolder' => $defaultPkgFolder . 'onegram',
		'classesFolder' => $defaultClassesFolder,
		'active' => true
	],
];
