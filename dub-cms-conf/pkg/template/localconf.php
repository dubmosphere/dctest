<?php
return [
	'vendor' => 'Dub',
	'cssFiles' => [
		$GLOBALS['dub']['packages']['template']['packageFolder'] . '/css/lib/bootstrap.min.css',
		$GLOBALS['dub']['packages']['template']['packageFolder'] . '/css/lib/bootstrap-theme.min.css',
		$GLOBALS['dub']['packages']['template']['packageFolder'] . '/css/style.css',
	],
	'jsFilesHead' => [
		$GLOBALS['dub']['packages']['template']['packageFolder'] . '/js/lib/jquery-2.2.3.min.js',
		$GLOBALS['dub']['packages']['template']['packageFolder'] . '/js/lib/bootstrap.min.js',
	],
	'jsFilesBody' => [
		[
			'file' => $GLOBALS['dub']['packages']['template']['packageFolder'] . '/js/main.js',
			'async' => true,
		],
	],
];
