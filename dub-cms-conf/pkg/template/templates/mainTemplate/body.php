<div class="allWrap">
	<div class="contentWrap">
		<div class="header">
			<h1>Onegram</h1>
		</div>
		<div class="contentColumns">
			<?php
			$this -> renderPackageTemplate(
				'onegram',
				$this->get('packageInfo')['onegram']['controller'],
				$this->get('packageInfo')['onegram']['action']
			);
			?>
		</div>
		<div class="footer">
			<p>&copy; 2016 Simon H&auml;sler</p>
		</div>
	</div>
</div>