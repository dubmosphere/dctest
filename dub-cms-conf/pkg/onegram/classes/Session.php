<?php
namespace Dub\Onegram;

class Session {
	public static function create($user = array()) {
		$_SESSION['user'] = array();
		$_SESSION['user']['uid'] = $user['uid'];
		$_SESSION['user']['username'] = $user['username'];
		$_SESSION['user']['e_mail'] = $user['e_mail'];
		$_SESSION['csrf'] = sha1(microtime());
	}
	
	public static function destroy() {
		session_destroy();
		unset($_SESSION['user']);
		unset($_SESSION['csrf']);
	}
	
	public static function isLoggedin($userUid = null) {
		if($userUid == null) {
			return isset($_SESSION['user']);
		}
		
		return isset($_SESSION['user']) && $_SESSION['user']['uid'] == $userUid;
	}
}
