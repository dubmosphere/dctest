<?php
namespace Dub\Onegram\Model;

/**
 * The User Model
 */
class Tag extends \Dub\Core\Model\AbstractModel {
	protected $table = 'pkg_onegram_tag';

	/**
	 * Find tag by name
	 * 
	 * @param string $tagName
	 * 
	 * @return array The query result
	 */
	public function findByName($tagName) {
		return $this -> db -> selectSingle($this -> table, '*', 'name = ?', array($tagName));
	}

	/**
	 * Find by image uid
	 * 
	 * @return array The query result
	 */
	public function findByImageUid($imageUid) {
		return $this -> db -> selectMm(
			$this -> table,
			'*',
			'pkg_onegram_image_tag AS rel ON ' . $this -> table . '.uid = rel.tag_uid',
			'rel.image_uid = ?',
			array($imageUid)
		);
	}
}