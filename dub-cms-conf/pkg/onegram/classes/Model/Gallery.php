<?php
namespace Dub\Onegram\Model;

/**
 * The User Model
 */
class Gallery extends \Dub\Core\Model\AbstractModel {
	protected $table = 'pkg_onegram_gallery';

	/**
	 * Find gallery by user uid
	 * 
	 * @param string $userUid
	 * 
	 * @return array The query result
	 */
	public function findByUserUid($userUid) {
		return $this -> db -> select($this -> table, '*', 'user_uid = ?', array($userUid));
	}
}