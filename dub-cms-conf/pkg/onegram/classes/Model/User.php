<?php
namespace Dub\Onegram\Model;

/**
 * The User Model
 */
class User extends \Dub\Core\Model\AbstractModel {
	protected $table = 'pkg_onegram_user';

	/**
	 * Find user by username
	 * 
	 * @param string $username
	 * 
	 * @return array The query result
	 */
	public function findByUsernameOrEmail($username, $email = null) {
		if($email == null) {
			$email = $username;
		}
		
		return $this -> db -> selectSingle($this -> table, '*', 'username = ? OR e_mail = ?', array($username, $email));
	}

	/**
	 * Insert an user
	 * 
	 * @param array $user
	 * 
	 * @return int The inserted uid
	 */
	public function insertUser($user = array()) {
		if(empty($this -> findByUsernameOrEmail($user['username'], $user['e_mail']))) {
			return $this -> insert($user);
		}
		
		return false;
	}

	/**
	 * Update a users password
	 * 
	 * @param array $user
	 * 
	 * @return bool Success
	 */
	public function updatePassword($user = array()) {
		return $this -> db -> update($this -> table, 'password', $user['password'], 'uid = ?', array($user['uid']));
	}
}