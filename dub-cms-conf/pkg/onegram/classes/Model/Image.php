<?php
namespace Dub\Onegram\Model;

/**
 * The User Model
 */
class Image extends \Dub\Core\Model\AbstractModel {
	protected $table = 'pkg_onegram_image';

	/**
	 * Find image by gallery uid
	 * 
	 * @param int $galleryUid
	 * 
	 * @return array The query result
	 */
	public function findByGalleryUid($galleryUid) {
		return $this -> db -> select($this -> table, '*', 'gallery_uid = ?', array($galleryUid));
	}

	/**
	 * Find all images with given tags
	 * 
	 * @param int $galleryUid
	 * @param array $tags
	 * 
	 * @return array The query result
	 */
	public function findByGalleryUidAndTagUids($galleryUid, $tagUids = array(), $mode = 'AND') {
		$join = '';
		$where = '';
		
		foreach($tagUids as $i => $tag) {
			$join .= ($i == 0 ? '' : ' JOIN ') . 'pkg_onegram_image_tag AS rel' . $i . ' ON ' . $this -> table . '.uid = ' . 'rel' . $i .'.image_uid';
			$where .= ($i == 0 ? '' : ' ' . $mode . ' ') . 'rel' . $i . '.tag_uid = ?';
		}
		
		return $this -> db -> selectMm(
			$this -> table,
			'*',
			$join,
			$where . ' AND ' . $this -> table . '.gallery_uid = ? GROUP BY ' . $this -> table . '.uid',
			array_merge($tagUids, array($galleryUid))
		);
	}

	/**
	 * Insert an image/tag relation
	 * 
	 * @param array $imageUid
	 * @param array $tagUid
	 * 
	 * @return int The inserted uid
	 */
	public function insertImageTagRelation($imageUid, $tagUid) {
		return $this -> db -> insert('pkg_onegram_image_tag', array('image_uid' => $imageUid, 'tag_uid' => $tagUid));
	}

	/**
	 * Delete an image/tag relation
	 * 
	 * @param array $imageUid
	 * 
	 * @return bool Success
	 */
	public function deleteImageTagRelation($imageUid) {
		return $this -> db -> delete('pkg_onegram_image_tag', 'image_uid = ?', array($imageUid));
	}
}