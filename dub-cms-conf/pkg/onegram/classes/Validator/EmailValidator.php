<?php
namespace Dub\Onegram\Validator;

class EmailValidator implements \Dub\Core\Validator\ValidatorInterface {
	public function isValid($value, $options = array()) {
		return filter_var($value, FILTER_VALIDATE_EMAIL);
	}
}
