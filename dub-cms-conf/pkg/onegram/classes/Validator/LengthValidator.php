<?php
namespace Dub\Onegram\Validator;

class LengthValidator implements \Dub\Core\Validator\ValidatorInterface {
	public function isValid($value, $options = array()) {
		return strlen($value) >= $options['length'];
	}
}
