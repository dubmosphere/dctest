<?php
namespace Dub\Onegram\Validator;

class PasswordValidator implements \Dub\Core\Validator\ValidatorInterface {
	public function isValid($value, $options = array()) {
		$hasLowercase = $hasUppercase = $hasNumber = $hasSpecialChar = false;
		
		for($i = 0; $i < strlen($value); ++$i) {
			if(preg_match('/[a-z]/', $value[$i])) {
				$hasLowercase = true;
			}
			if(preg_match('/[A-Z]/', $value[$i])) {
				$hasUppercase = true;
			}
			if(preg_match('/[0-9]/', $value[$i])) {
				$hasNumber = true;
			}
			if(preg_match('/[#§$%&+!\\-_@|]/', $value[$i])) {
				$hasSpecialChar = true;
			}
		}
		
		return $hasLowercase && $hasUppercase && $hasNumber && $hasSpecialChar;
	}
}
