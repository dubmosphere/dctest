<?php
namespace Dub\Onegram\Validator;

class RequiredValidator implements \Dub\Core\Validator\ValidatorInterface {
	public function isValid($value, $options = array()) {
		return $value != '';
	}
}
