<?php
namespace Dub\Onegram\Controller;

class UserController extends \Dub\Core\Controller\AbstractController {
	private $userModel;
	private $pepper = 'r4N$a#h+ar13aC90jKl';

	protected function init() {
		$this -> userModel = new \Dub\Onegram\Model\User();
	}

	/**
	 * Login controller action handels the login
	 */
	public function loginAction() {
		$loggedin = \Dub\Onegram\Session::isLoggedin();
		
		if(!$loggedin) {
			if(isset($_POST['submit'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				// Validate the login
				$user = $this -> validateLogin($username, $password);
				
				// Unset the plain password
				unset($password);
				
				// If no error occured, login
				if(empty($this -> errors)) {
					$this -> login($user);
				} else {
					$this -> view -> assign('user', $user);
				}
	 		}
	 		
			$this -> view -> assign('errors', $this -> errors);
		} else {
			\Dub\Core\Utility\LinkUtility::redirect('onegram', 'user', 'show', array($_SESSION['user']['uid']));
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}

	/**
	 * Registration controller action handels the registration
	 */
	public function registrationAction() {
		$loggedin = \Dub\Onegram\Session::isLoggedin();
		
		if(!$loggedin) {
			if(isset($_POST['submit'])) {
				$user = array();
				$user['username'] = $_POST['username'];
				$user['e_mail'] = $_POST['e_mail'];
				$user['password'] = $_POST['password'];
				$user['passwordConfirm'] = $_POST['passwordConfirm'];
				
				// Validate the registration
				$this -> validateRegistration($user);
	
				// Unset the plain confirmation password
				unset($user['passwordConfirm']);
				
				// If no errors, insert the user and login
				if(!$this -> errors) {
					$user['uid'] = $this -> userModel -> insertUser($user);
					
					if($user['uid']) {
						$this -> login($user);
					}
				} else {
					$this -> view -> assign('user', $user);
				}
			}
	 		
			$this -> view -> assign('errors', $this -> errors);
		} else {
			\Dub\Core\Utility\LinkUtility::redirect('onegram', 'user', 'show', array($_SESSION['user']['uid']));
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}

	/**
	 * Logout controller action handels the logout
	 */
	public function logoutAction() {
		\Dub\Onegram\Session::destroy();
	}

	/**
	 * Show action shows a user by its username
	 * 
	 * @param int $uid
	 */
	public function showAction($userUid) {
		$loggedin = \Dub\Onegram\Session::isLoggedin($userUid);
		
		if($loggedin) {
			$user = $this -> userModel -> findByUid($userUid);
			$this -> view -> assign('user', $user);
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}
	
	/**
	 * Validates the login data
	 * 
	 * @param string $username
	 * @param string $password
	 * 
	 * @return array $user The logged in user or an empty array
	 */
	protected function validateLogin($username, $password) {
		$user = array();
		
		// Check if a username is entered and get it from the database. You can enter the username or the email address
		if($username != '') {
			$user = $this -> userModel -> findByUsernameOrEmail($username);
			
			if(!$user) {
				$this -> errors['invalidUser'] = 'User does not exist';
			}
		} else {
			$this -> errors['noUsername'] = 'Enter a username';
		}
		
		// Check if a password is entered and if so verify it. Rehash the password if needed
		if($password != '') {
			$wrongPasswordMessage = 'The password you entered is wrong';
		 	if($user && !password_verify($password . $this -> pepper, $user['password'])) {
				$this -> errors['passwordIncorrect'] = $wrongPasswordMessage;
			} else {
				if(password_needs_rehash($user['password'], PASSWORD_BCRYPT)) {
			 		if(!password_verify($password . $this -> pepper, $user['password'])) {
						$this -> errors['passwordIncorrect'] = $wrongPasswordMessage;
			 		} else {
						$user['password'] = password_hash($password . $this -> pepper, PASSWORD_BCRYPT);
						$this -> userModel -> updatePassword($user);
			 		}
				}
			}
		} else {
			$this -> errors['noPassword'] = 'Enter a password';
		}
		
		return $user;
	}
	
	/**
	 * Validates the registration data of a user to register
	 * 
	 * @param array &$user
	 */
	protected function validateRegistration(&$user = array()) {
		// Validators
		$lengthValidator = new \Dub\Onegram\Validator\LengthValidator();
		$emailValidator = new \Dub\Onegram\Validator\EmailValidator();
		$passwordValidator = new \Dub\Onegram\Validator\PasswordValidator();
		$requiredValidator = new \Dub\Onegram\Validator\RequiredValidator();
		
		// Check username length
		if(!$lengthValidator -> isValid($user['username'], array('length' => 4))) {
			$this -> errors['invalidUsername'] = 'Username has to be at least 4 characters long';
		}
		
		if(!$emailValidator -> isValid($user['e_mail'])) {
			$this -> errors['invalidEmail'] = 'Enter a valid email address';
		}

		// Check if the username exists
		if($this -> userModel -> findByUsernameOrEmail($user['username'])) {
			$this -> errors['usernameAlreadyExists'] = 'User with this username already exists';
		}

		// Check if the email exists
		if($this -> userModel -> findByUsernameOrEmail($user['e_mail'])) {
			$this -> errors['emailAlreadyExists'] = 'User with this email address already exists';
		}

		// Check password length
		if(!$lengthValidator -> isValid($user['password'], array('length' => 6))) {
			$this -> errors['passwordNotLongEnough'] = 'Password has to be at least 6 characters long';
		}
		
		// Check if password is valid
		if(!$passwordValidator -> isValid($user['password'])) {
			$this -> errors['invalidPassword'] = 'Password must contain at least one uppercase and one lowercase letter, a number and a specialchar';
		}

		// Check if a password confirmation is entered
		if(!$requiredValidator -> isValid($user['passwordConfirm'])) {
			$this -> errors['noPasswordConfirm'] = 'Confirm your password';
		}

		// Check if the password and the confirmation match
		if($user['password'] != $user['passwordConfirm']) {
			$this -> errors['passwordsDontMatch'] = 'Passwords don\'t match';
			
			// Unset the plain password
			unset($user['password']);
		} else {
			$user['password'] = password_hash($user['password'] . $this -> pepper, PASSWORD_BCRYPT);
		}
	}
	
	/**
	 * Perform the login
	 * 
	 * @param array $user
	 */
	protected function login($user = array()) {
		\Dub\Onegram\Session::create($user);
		\Dub\Core\Utility\LinkUtility::redirect('onegram', 'user', 'show', array($user['uid']));
	}
}
