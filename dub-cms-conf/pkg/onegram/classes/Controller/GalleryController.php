<?php
namespace Dub\Onegram\Controller;

class GalleryController extends \Dub\Core\Controller\AbstractController {
	private $galleryModel = null;
	private $imageModel = null;
	private $tagModel = null;
	private $isFiltered = false;

	protected function init() {
		$this -> galleryModel = new \Dub\Onegram\Model\Gallery();
		$this -> imageModel = new \Dub\Onegram\Model\Image();
		$this -> tagModel = new \Dub\Onegram\Model\Tag();
	}
	
	public function listAction($userUid) {
		$loggedin = \Dub\Onegram\Session::isLoggedin($userUid);
		
		if($loggedin) {
			$galleries = $this ->  galleryModel -> findByUserUid($userUid);
			
			foreach($galleries as &$gallery) {
				$gallery['images'] = $this -> imageModel -> findByGalleryUid($gallery['uid']);
			}
			
			$this -> view -> assign('userUid', $userUid);
			$this -> view -> assign('galleries', $galleries);
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}
	
	public function showAction($galleryUid) {
		$gallery = $this -> galleryModel -> findByUid($galleryUid);
		$loggedin = \Dub\Onegram\Session::isLoggedin($gallery['user_uid']);
		
		if($loggedin) {
			$gallery['images'] = $this -> filter($gallery['uid']);
			
			$this -> view -> assign('gallery', $gallery);
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}
	
	public function diashowAction($galleryUid) {
		$gallery = $this -> galleryModel -> findByUid($galleryUid);
		$loggedin = \Dub\Onegram\Session::isLoggedin($gallery['user_uid']);
		
		if($loggedin) {
			$gallery['images'] = $this -> filter($gallery['uid']);
			$activeImgFound = false;
			
			foreach($gallery['images'] as &$image) {
				$image['uid'] = $this -> isFiltered ? $image['image_uid'] : $image['uid'];
				$image['tags'] = $this -> tagModel -> findByImageUid($image['uid']);
				
				if(isset($_GET['active'])) {
					$active = $_GET['active'];
					if($image['uid'] == $active) {
						$image['active'] = true;
						$activeImgFound = true;
					} else {
						$image['active'] = false;
					}
				}
			}
			
			$this -> view -> assign('activeImgFound', $activeImgFound);
			$this -> view -> assign('gallery', $gallery);
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}
	
	public function createAction($userUid) {
		$loggedin = \Dub\Onegram\Session::isLoggedin($userUid);
		
		if($loggedin) {
			if(isset($_POST['submit'])) {
				$gallery = array();
				$gallery['name'] = $_POST['name'];
				$gallery['description'] = $_POST['description'];
				$gallery['user_uid'] = $userUid;
				
				if($gallery['name'] == '') {
					$this -> errors['noName'] = 'Enter a name';
				}
				
				if($gallery['description'] == '') {
					$this -> errors['noDescription'] = 'Enter a description';
				}
				
				if(empty($this -> errors)) {
					$gallery['uid'] = $this -> galleryModel -> insert($gallery);
					\Dub\Core\Utility\LinkUtility::redirect('onegram', 'gallery', 'show', array($gallery['uid']));
				} else {
					$this -> view -> assign('gallery', $gallery);
				}
			}
			
			$this -> view -> assign('errors', $this -> errors);
			$this -> view -> assign('userUid', $userUid);
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}
	
	private function filter($galleryUid) {
		$filter = '';
		$images = array();
		$tags = array();
		
		if(isset($_GET['filter'])) {
			$filter = $_GET['filter'];
			if($filter != '') {
				$filter = explode(' ', $filter);
				
				foreach ($filter as $tagName) {
					$tag = $this -> tagModel -> findByName($tagName);
					if(!empty($tag)) {
						$tags[] = $tag['uid'];
					}
				}
				$images = $this -> imageModel -> findByGalleryUidAndTagUids($galleryUid, $tags);
				$this -> isFiltered = true;
				$filter = implode(' ', $filter);
			}
		}
		
		if(!$this -> isFiltered) {
			$images = $this -> imageModel -> findByGalleryUid($galleryUid);
		}
		
		$this -> view -> assign('filter', $filter);
		$this -> view -> assign('tags', $tags);
		
		return $images;
	}
}
