<?php
namespace Dub\Onegram\Controller;

class ImageController extends \Dub\Core\Controller\AbstractController {
	private $imageModel = null;
	private $galleryModel = null;
	private $tagModel = null;

	protected function init() {
		$this -> imageModel = new \Dub\Onegram\Model\Image();
		$this -> galleryModel = new \Dub\Onegram\Model\Gallery();
		$this -> tagModel = new \Dub\Onegram\Model\Tag();
	}
	
	public function showAction($imageUid) {
		$image = $this -> imageModel -> findByUid($imageUid);
		$gallery = $this -> galleryModel -> findByUid($image['gallery_uid']);
		$loggedin = \Dub\Onegram\Session::isLoggedin($gallery['user_uid']);
		
		if($loggedin) {
			$tags = $this -> tagModel -> findByImageUid($image['uid']);
			$filter = '';
			
			if(isset($_GET['filter'])) {
				$filter = $_GET['filter'];
			}
			
			$this -> view -> assign('filter', $filter);
			$this -> view -> assign('image', $image);
			$this -> view -> assign('gallery', $gallery);
			$this -> view -> assign('tags', $tags);
		}

		$this -> view -> assign('loggedin', $loggedin);
	}
	
	public function uploadAction($galleryUid) {
		$gallery = $this -> galleryModel -> findByUid($galleryUid);
		$loggedin = \Dub\Onegram\Session::isLoggedin($gallery['user_uid']);
		
		if($loggedin) {
			if(isset($_POST['submit'])) {
				$image = array();
				$image['name'] = $_POST['name'];
				$image['description'] = $_POST['description'];
				$image['gallery_uid'] = $gallery['uid'];
				$tags = explode(' ', $_POST['tags']);
				
				if($image['name'] == '') {
					$this -> errors['noName'] = 'Enter a name';
				}
				
				if($image['description'] == '') {
					$this -> errors['noDescription'] = 'Enter a description';
				}
				
				$file = $_FILES['image'];
				
				if($file['name'] == '') {
					$this -> errors['noFile'] = 'Choose a file to upload';
				}
		
				if(empty($this -> errors)) {
					$pathInfo = pathinfo($file['name']);
					
					$target = $this -> createFolderStructure($gallery);
					$target .= str_replace('.', '_', basename($file['name'], $pathInfo['extension'])) . uniqid() . '.' . $pathInfo['extension'];
					$image['source'] = $target;
					
					if(move_uploaded_file($_FILES["image"]["tmp_name"], $image['source'])) {
						$image['thumbnail'] = $this -> generateThumbnail($image, $gallery);
						
						if(empty($this -> errors)) {
							$image['uid'] = $this -> imageModel -> insert($image);
						
							$this -> createImageTagRelations($image, $tags);
							
							\Dub\Core\Utility\LinkUtility::redirectByUrl(
								\Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'diashow', array($image['gallery_uid'])) . '?active=' . $image['uid']
							);
						}
					} else {
						$this -> errors['uploadFailed'] = 'Could not upload image';
					}
				}
				if(!empty($this -> errors)) {
					$this -> view -> assign('tags', implode(' ', $tags));
					$this -> view -> assign('image', $image);
				}
			}
			
			$this -> view -> assign('errors', $this -> errors);
			$this -> view -> assign('gallery', $gallery);
		}

		$this -> view -> assign('loggedin', $loggedin);
	}

	public function deleteAction($imageUid) {
		$image = $this -> imageModel -> findByUid($imageUid);
		$gallery = $this -> galleryModel -> findByUid($image['gallery_uid']);
		$loggedin = \Dub\Onegram\Session::isLoggedin($gallery['user_uid']);
		
		if($loggedin) {
			unlink($image['source']);
			unlink($image['thumbnail']);
		
			$this -> imageModel -> deleteImageTagRelation($imageUid);
			$this -> imageModel -> delete($imageUid);
			
			\Dub\Core\Utility\LinkUtility::redirect('onegram', 'gallery', 'show', array($image['gallery_uid']));
		}
		
		$this -> view -> assign('loggedin', $loggedin);
	}

	private function generateThumbnail($image = array(), $gallery = array(), $desiredWidth = 100) {
		$extension = pathinfo($image['source'], PATHINFO_EXTENSION);
		
		$target = $this -> createFolderStructure($gallery, 'thumbnails');
		
		$target .= 'thumb_' . $gallery['user_uid'] . '_' . $gallery['uid'] . '_' . $image['name'] . '_' . uniqid() . time() . '.jpg';

		switch($extension) {
			case 'png':
				$sourceImage = imagecreatefrompng($image['source']);
				break;
			case 'jpeg':
			case 'jpg':
				$sourceImage = imagecreatefromjpeg($image['source']);
				break;
			case 'gif':
				$sourceImage = imagecreatefromgif($image['source']);
				break;
			default:
				$this -> errors['wrongMimeType'] = 'We only support ".png", ".jpeg", ".jpg" and ".gif" Files';
				break;
		}

		$width = imagesx($sourceImage);
		$height = imagesy($sourceImage);
		$desiredHeight = floor($height * ($desiredWidth / $width));
		$virtualImage = imagecreatetruecolor($desiredWidth, $desiredHeight);
		
		imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $desiredWidth, $desiredHeight, $width, $height);
		imagejpeg($virtualImage, $target);
		return $target;
	}
	
	private function createImageTagRelations($image, $tags) {
		foreach($tags as $tagName) {
			if($tagName != '') {
				$tag = $this -> tagModel -> findByName($tagName);
				
				if(empty($tag)) {
					$tag = array();
					$tag['name'] = $tagName;
					$tag['uid'] = $this -> tagModel -> insert($tag);
				}
				
				$success = $this -> imageModel -> insertImageTagRelation($image['uid'], $tag['uid']);
			}
		}
	}

	private function createFolderStructure($gallery = array(), $startFolder = 'uploads') {
		$target = $startFolder . DS;
		
		if(!file_exists($target)) {
			mkdir($target);
		}
		
		$target .= 'user' . $gallery['user_uid'] . DS;
		
		if(!file_exists($target)) {
			mkdir($target);
		}
		
		$target .= 'gallery' . $gallery['uid'] . DS;
		
		if(!file_exists($target)) {
			mkdir($target);
		}
		
		return $target;
	}
}
