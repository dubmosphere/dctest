<?php
return [
	'vendor' => 'Dub',
	'defaultController' => 'User',
	'defaultAction' => 'login',
	//'hasOwnAutoloader' => true,
	'cssFiles' => [
		$GLOBALS['dub']['packages']['onegram']['packageFolder'] . '/css/style.css',
	],
	'jsFilesBody' => [
		[
			'file' => $GLOBALS['dub']['packages']['onegram']['packageFolder'] . '/js/gallery.js',
			'async' => true,
		],
		[
			'file' => $GLOBALS['dub']['packages']['onegram']['packageFolder'] . '/js/onegram.js',
			'async' => true,
		],
	],
];
