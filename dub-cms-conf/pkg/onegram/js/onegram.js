$(document).ready(function() {
	var gallery = new Gallery('.onegramGalleryDiashow .onegramGalleryImages .onegramImageSingle');
	var autoplay = true;
	
	setInterval(function() {
		if(autoplay == true) {
			gallery.next();
		}
	}, 10000);
	
	$('.onegramGalleryDiashow .onegramGalleryImages .prev').click(function() {
		gallery.prev();
		autoplay = false;
	});
	
	$('.onegramGalleryDiashow .onegramGalleryImages .next').click(function() {
		gallery.next();
		autoplay = false;
	});
});
