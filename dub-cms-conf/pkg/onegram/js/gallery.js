Gallery = function(imageSelector, activeClass) {
	var $activeImage;
	
	// Constructor
	(function() {
		// Check the constructor parameters
		if(!imageSelector) throw 'Gallery(imageSelector, activeClass): No imageSelector set';
		activeClass = activeClass || 'active';
		
		// Private
		$activeImage = $(imageSelector + '.' + activeClass);
		if($activeImage.length == 0) {
			$activeImage = $(imageSelector).first();
			$activeImage.addClass(activeClass);
		}
	})();
	
	/**
	 * Change the image according to the direction
	 * 
	 * @param string direction
	 */
	var changeImage = function(direction) {
		// Remove classes
		$activeImage.removeClass(activeClass);
		
		// Swap images
		if(direction == 'prev') {
			changeImagePrev();
		} else if(direction == 'next') {
			changeImageNext();
		} else {
			throw 'Gallery::changeImage(direction): The direction "' + direction + '" does not exist';
		}
		
		$activeImage.addClass(activeClass);
	}
	
	/**
	 * Change the image to the previous image
	 */
	var changeImagePrev = function() {
		var $prevImage = $activeImage.prev(imageSelector);
		
		if($prevImage.length == 0) {
			$activeImage = $(imageSelector).last();
		} else {
			$activeImage = $prevImage;
		}
	}
	
	/**
	 * Change the image to the next image
	 */
	var changeImageNext = function() {
		var $nextImage = $activeImage.next(imageSelector);
		
		if($nextImage.length == 0) {
			$activeImage = $(imageSelector).first();
		} else {
			$activeImage = $nextImage;
		}
	}

	// Public
	this.prev = function() {
		changeImage('prev');
	}

	this.next = function() {
		changeImage('next');
	}
}
