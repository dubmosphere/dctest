<div class="onegramGalleryListElement">
	<h3><a href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'show', array($gallery['uid'])) ?>"><?= $gallery['name'] ?></a></h3>
	<p><?= nl2br($gallery['description']) ?></p>
</div>