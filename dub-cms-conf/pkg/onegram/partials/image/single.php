<div id="onegramImg<?= $image['uid'] ?>" class="onegramImageSingle<?= ((isset($image['active']) && $image['active']) || (!$this -> get('activeImgFound') && $index == 0) ? ' active' : '') ?>">
	<div class="onegramImage">
		<img src="/<?= $image['source'] ?>" alt="<?= $image['name'] ?>" />
	</div>
	<?php if(!empty($image['tags'])): ?>
		<div class="tags">
			<?php foreach($image['tags'] as $tag): ?>
				<span class="tag"><?= $tag['name'] ?></span>
			<?php endforeach ?>
		</div>
	<?php endif; ?>
	<?php if(isset($showDescription) && $showDescription): ?>
		<div class="onegramImageDescription">
			<span><?= nl2br($image['description']) ?></span>
		</div>
	<?php endif; ?>
	<div class="onegramImageDeleteLink">
		<a onclick="return confirm('Are you sure you want to delete this item?');" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'image', 'delete', array($image['uid'])) ?>">Delete</a>
	</div>
</div>