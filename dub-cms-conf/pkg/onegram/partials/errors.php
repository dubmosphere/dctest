<?php if(!empty($errors)): ?>
	<div class="errors">
		<?php foreach($errors as $error): ?>
			<div class="error alert alert-danger">
				<a class="close" href="#" data-dismiss="alert" aria-label="close">&times;</a>
				<?= $error ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>