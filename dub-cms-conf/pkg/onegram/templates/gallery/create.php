<div class="onegramGalleryCreation">
	<h2>Create new gallery</h2>
	<?php if($this -> get('loggedin')): ?>
		<?php $this -> renderPartial('onegram', 'errors', array('errors' => $this -> get('errors'))); ?>
		<form action="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'create', array($this -> get('userUid'))) ?>" method="post">
			<input type="hidden" name="csrf" value="<?= $_SESSION['csrf'] ?>" />
			<fieldset class="form-group">
				<input type="text" name="name" value="<?= $this -> get('gallery')['name'] ?>" placeholder="Gallery name" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<textarea name="description" placeholder="Gallery description" class="form-control"><?= $this -> get('gallery')['description'] ?></textarea>
			</fieldset>
			<fieldset class="form-group">
				<input type="submit" name="submit" value="Create" class="btn btn-primary" /><!--
				--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'list', array($this -> get('userUid'))) ?>">Show galleries</a>
			</fieldset>
		</form>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>