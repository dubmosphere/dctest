<div class="onegramGalleryList">
	<h2>Gallery list</h2>
	<?php if($this -> get('loggedin')): ?>
		<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'create', array($this -> get('userUid'))) ?>">Create gallery</a><!--
		--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'show', array($this -> get('userUid'))) ?>">Go to user page</a>
		<?php if(!empty($this -> get('galleries'))): ?>
			<?php foreach($this -> get('galleries') as $gallery): ?>
				<?php $this -> renderPartial('onegram', 'gallery/listElement', array('gallery' => $gallery)); ?>
			<?php endforeach; ?>
		<?php else: ?>
			<p>No galleries found...</p>
		<?php endif; ?>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>