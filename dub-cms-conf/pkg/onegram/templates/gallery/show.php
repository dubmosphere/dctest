<div class="onegramGalleryShow">
	<?php if($this -> get('loggedin')): ?>
		<h2><?= $this -> get('gallery')['name'] ?></h2>
		<p><?= nl2br($this -> get('gallery')['description']) ?></p>
		<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'image', 'upload', array($this -> get('gallery')['uid'])) ?>">Upload new image</a><!--
		--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'list', array($this -> get('gallery')['user_uid'])) ?>">Back to gallery list</a><!--
		--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'diashow', array($this -> get('gallery')['uid'])) ?>">Start diashow</a>
		<form action="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'show', array($this -> get('gallery')['uid'])) ?>" method="get">
			<input type="text" name="filter" placeholder="Search" value="<?= $this -> get('filter') ?>" /><input type="submit" value="Search" class="btn btn-primary" />
		</form>
		<div class="thumbnails">
			<?php if(!empty($this -> get('gallery')['images'])): ?>
				<?php foreach($this -> get('gallery')['images'] as $image): ?><!--
					--><?php $this -> renderPartial('onegram', $this -> get('filter') != '' ? 'image/thumbnailFiltered' : 'image/thumbnail', array('image' => $image)); ?><!--
				--><?php endforeach; ?>
			<?php else: ?>
				<p>No images found...</p>
			<?php endif; ?>
		</div>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>