<div class="onegramGalleryDiashow">
	<?php if($this -> get('loggedin')): ?>
		<h2><?= $this -> get('gallery')['name'] ?></h2>
		<p><?= nl2br($this -> get('gallery')['description']) ?></p>
		<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'show', array($this -> get('gallery')['uid'])) . ($this -> get('filter') != '' ? '?filter=' . $this -> get('filter') : '') ?>">Back to gallery</a>
		<?php if(!empty($this -> get('gallery')['images'])): ?>
			<div class="onegramGalleryImages">
				<div class="images">
					<?php $i = 0; ?>
					<?php foreach($this -> get('gallery')['images'] as $image): ?>
						<?php $this -> renderPartial('onegram', 'image/single', array('image' => $image, 'showDescription' => true, 'index' => $i)); ?>
						<?php ++$i; ?>
					<?php endforeach; ?>
					<div class="aspectRatio"></div>
				</div>
				<div class="prev"></div>
				<div class="next"></div>
			</div>
		<?php else: ?>
			<p>No images found...</p>
		<?php endif; ?>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>