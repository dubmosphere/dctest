<div class="onegramImageShow">
	<?php if($this -> get('loggedin')): ?>
		<h2><?= $this -> get('gallery')['name'] ?></h2>
		<h3><?= $this -> get('image')['name'] ?></h3>
		<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'show', array($this -> get('gallery')['uid'])) . ($this -> get('filter') != '' ? '?filter=' . $this -> get('filter') : '') ?>">Back to gallery</a>
		<div class="image">
			<?php $this -> renderPartial('onegram', 'image/single', array('image' => $this -> get('image'))) ?>
			<div class="aspectRatio"></div>
		</div>
		<div class="description">
			<p><?= nl2br($this -> get('image')['description']) ?></p>
		</div>
		<?php if(!empty($this -> get('tags'))): ?>
			<div class="tags">
				<?php foreach($this -> get('tags') as $tag): ?>
					<span class="tag"><?= $tag['name'] ?></span>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>