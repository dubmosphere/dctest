<div class="onegramImageUpload">
	<h2>Upload a new image</h2>
	<?php if($this -> get('loggedin')): ?>
		<?php $this -> renderPartial('onegram', 'errors', array('errors' => $this -> get('errors'))); ?>
		<form action="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'image', 'upload', array($this -> get('gallery')['uid'])) ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="csrf" value="<?= $_SESSION['csrf'] ?>" />
			<fieldset class="form-group">
				<input type="text" name="name" value="<?= $this -> get('image')['name'] ?>" placeholder="Image name" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<textarea name="description" placeholder="Image description" class="form-control"><?= $this -> get('image')['description'] ?></textarea>
			</fieldset>
			<fieldset class="form-group">
				<input type="file" name="image" accept="image/*" />
			</fieldset>
			<fieldset class="form-group">
				<input type="text" name="tags" value="<?= $this -> get('tags') ?>" placeholder="Image tags (Separated with a space)" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<input type="submit" name="submit" value="Upload" class="btn btn-primary" /><!--
				--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'show', array($this -> get('gallery')['uid'])) ?>">Back to gallery</a>
			</fieldset>
		</form>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>