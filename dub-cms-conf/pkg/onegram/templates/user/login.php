<div class="onegramLogin">
	<h2>Login</h2>
	<?php if(!$this -> get('loggedin')): ?>
		<?php $this -> renderPartial('onegram', 'errors', array('errors' => $this -> get('errors'))); ?>
		<form action="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'login') ?>" method="post">
			<fieldset class="form-group">
				<input type="text" name="username" placeholder="Username or email" class="form-control" value="<?= $this -> get('user')['username'] ?>" />
			</fieldset>
			<fieldset class="form-group">
				<input type="password" name="password" placeholder="Password" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<input type="submit" name="submit" value="Login" class="btn btn-primary" /><!--
				--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'registration') ?>">Go to the Registration</a>
			</fieldset>
		</form>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/alreadyLoggedin'); ?>
	<?php endif; ?>
</div>