<div class="onegramUser">
	<?php if($this -> get('loggedin')): ?>
		<h2><?= $this -> get('user')['username'] ?></h2>
		<p>Username: <?= $this -> get('user')['username'] ?></p>
		<p>Email: <?= $this -> get('user')['e_mail'] ?></p>
		<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'logout') ?>">Logout</a><!--
		--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'gallery', 'list', array($this -> get('user')['uid'])) ?>">Show galleries</a>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/loginFirst'); ?>
	<?php endif; ?>
</div>