<div class="onegramRegistration">
	<h2>Registration</h2>
	<?php if(!$this -> get('loggedin')): ?>
		<?php $this -> renderPartial('onegram', 'errors', array('errors' => $this -> get('errors'))); ?>
		<form action="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'registration') ?>" method="post">
			<fieldset class="form-group">
				<input type="text" name="username" placeholder="Username *" class="form-control" value="<?= $this -> get('user')['username'] ?>" />
			</fieldset>
			<fieldset class="form-group">
				<input type="text" name="e_mail" placeholder="E-Mail *" class="form-control" value="<?= $this -> get('user')['e_mail'] ?>" />
			</fieldset>
			<fieldset class="form-group">
				<input type="password" name="password" placeholder="Password *" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<input type="password" name="passwordConfirm" placeholder="Confirm the Password *" class="form-control" />
			</fieldset>
			<fieldset class="form-group">
				<input type="submit" name="submit" value="Register" class="btn btn-primary" /><!--
				--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'login') ?>">Go to the Login</a>
			</fieldset>
		</form>
	<?php else: ?>
		<?php $this -> renderPartial('onegram', 'user/alreadyLoggedin'); ?>
	<?php endif; ?>
</div>