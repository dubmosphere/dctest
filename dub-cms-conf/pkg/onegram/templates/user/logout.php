<div class="onegramLogout">
	<h2>You are now logged out</h2>
	<a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'login') ?>">Login again</a><!--
	--><a class="btn btn-default" href="<?= \Dub\Core\Utility\LinkUtility::buildUrl('onegram', 'user', 'registration') ?>">Register a new account</a>
</div>