<?php
/**
 * Dub CMS
 * If not working, use PHP 5.3 or higher.
 */
// Shortcut DIRECTORY_SEPARATOR
define('DS', DIRECTORY_SEPARATOR);
define('DOCUMENT_ROOT', '..' . DS . '..' . DS . '..' . DS . '..' . DS);

// Include the frontend application class (This is only needed in classes loaded before the packages)
require_once DOCUMENT_ROOT . 'dub-cms-conf' . DS . 'pkg' . DS . 'onegram' . DS . 'classes'
	. DS . 'Install' . DS . 'Application' . DS . 'OnegramInstallApplication.php';

// Create the application and run it
$app = new \Dub\Onegram\Install\Application\OnegramInstallApplication();
$app -> run();
