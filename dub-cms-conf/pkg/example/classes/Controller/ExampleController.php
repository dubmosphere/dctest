<?php
namespace Dub\Example\Controller;

class ExampleController extends \Dub\Core\Controller\AbstractController {
	private $exampleModel;
	
	public function init() {
		$this -> exampleModel = new \Dub\Example\Model\Example();
	}
	
	public function exampleAction() {
		$variable = $this -> exampleModel -> example();
		
		$this -> view -> assign('variable', $variable);
	}
}
