<?php
function getCsv($file, $delimiter = ',') {
	if(!file_exists($file)) {
		touch($file);
		return array();
	}
	
	$fp = fopen($file, 'r');
	$data = array();
	
	$fields = fgetcsv($fp, null, $delimiter);
	
	while(($csv = fgetcsv($fp, null, $delimiter)) !== false) {
		if(!$csv[0])
			continue;
		
		$row = array();
		
		foreach($csv as $index => $value) {
			$row[$fields[$index]] = $value;
		}
	
		$data[] = $row;
	}
	
	return $data;
}

function writeCsv($file, $data, $append = false, $delimiter = ',') {
	$csv = '';
	
	foreach($data as $i => $row) {
		$fields = array_keys($row);
		
		if(!$append && $i == 0) {
			foreach($fields as $index => $field) {
				$csv .= $field . (($index >= count($fields) - 1) ? '' : $delimiter);
			}
			
			$csv .= "\n";
		}
		
		foreach(array_values($row) as $index => $value) {
			$csv .= $value . (($index >= count($fields) - 1) ? '' : $delimiter);
		}
		
		$csv .= "\n";
	}
	
	file_put_contents($file, $csv, $append ? FILE_APPEND : null);
}

function insertCsv($file, $data, $append = true, $delimiter = ',') {
	writeCsv($file, array($data), $append, $delimiter);
	
	return $data;
}

function debug($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}

// Controller
$users = getCsv('users.csv');

$user = array();
$user['username'] = 'username1';
$user['email'] = 'asdasdasdasda@aasdasd.cas';
$user['password'] = 'asdasdasdasda';
$users[] = insertCsv('users.csv', $user, !empty($users));
?>

<!-- The view -->
<?php foreach($users as &$user): ?>
	<div class="user" style="padding: 5px;">
		<?= $user['username'] ?><br>
		<?= $user['email'] ?><br>
		<?= $user['password'] ?><br>
	</div>
<?php endforeach; ?>

