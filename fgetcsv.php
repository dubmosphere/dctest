<?php
class Csv {
	private $filename;
	private $delimiter;
	
	public function __construct($filename, $delimiter = ',') {
		$this -> filename = $filename;
		$this -> delimiter = $delimiter;
	}
	
	public function read() {
		if(!file_exists($this -> filename)) {
			touch($this -> filename);
			return array();
		}
		
		$fp = fopen($this -> filename, 'r');
		$data = array();
		
		$fields = fgetcsv($fp, null, $this -> delimiter);
		
		while(($csv = fgetcsv($fp, null, $this -> delimiter)) !== false) {
			if(!$csv[0])
				continue;
			
			$row = array();
			
			foreach($csv as $index => $value) {
				$row[$fields[$index]] = $value;
			}
			
			$data[] = $row;
		}
		
		return $data;
	}
	
	public function write($data, $append = false) {
		$csv = '';
		
		foreach($data as $i => $row) {
			$fields = array_keys($row);
			
			if(!$append && $i == 0) {
				foreach($fields as $index => $field) {
					$csv .= $field . (($index >= count($fields) - 1) ? '' : $this -> delimiter);
				}
				
				$csv .= "\n";
			}
			
			foreach(array_values($row) as $index => $value) {
				$csv .= $value . (($index >= count($fields) - 1) ? '' : $this -> delimiter);
			}
			
			$csv .= "\n";
		}
		
		file_put_contents($this -> filename, $csv, $append ? FILE_APPEND : null);
	}
	
	public function insert($data, $append = true) {
		$this -> write(array($data), $append);
		
		return $data;
	}
}

function debug($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}

// Controller
$userModel = new Csv('users.csv');
$users = $userModel -> read();
/*
$user = array();
$user['username'] = 'username1';
$user['email'] = 'asdasdasdasda@aasdasd.cas';
$user['password'] = 'asdasdasdasda';
$users[] = $userModel -> insert($user, !empty($users));
*/
?>

<!-- The view -->
<?php foreach($users as &$user): ?>
	<div class="user" style="padding: 5px;">
		<?= $user['username'] ?><br>
		<?= $user['email'] ?><br>
		<?= $user['password'] ?><br>
	</div>
<?php endforeach; ?>

